# jwj-cloud-alibaba

#### 项目介绍 (本人格言:没有demo的架构都是耍流氓)
最近几年比较流行架构体系spring cloud alibaba,自己尝试的将原来的系统迁移到spring cloud alibaba，发现spring cloud alibaba确实蛮好用，特别是Nacos这个组件真的很方便，既能作为注册中心，服务治理，又能作为配置中心，而且还能配置文件动态修改，只需要搭建一套就能支持多环境部署，dev test prod 等，一个组件搞定原来spring cloud 需要的3个组件，确实很方便，原来越简化了我们维护和开发成本，谁用谁知道，这里我就不细化了。集成高可用流量管理框架Sentinel,zipkin链路调用监控组件。于是我就抽空将之前开源jwj-cloud-formwork架部分模块简单的进行了改造，于是就有了现在的这套架构脚手架，其中集成了knife4j自动化文档是swagger的升级版，支持网关不同模块的集成，让接口文档更加方便，还添加了自动化部署docker，这套估计是目前2021年终极版最完善的一套架构了，其它的组件可以自己集成，，写的不好的地方希望大家谅解！开源不易，且行且珍惜 :stuck_out_tongue: 如有问题可以加本人qq：619594586 qq群：894481200
### 环境要求
jdk8； mysql5.7以上； maven； redis； lombok ； sentinel ； zipkin ；docker ； nacos

### 项目结构


| 工程名称 | 模块            | 介绍                                             |
| -------- | --------------- | ------------------------------------------------ |
| 核心公共依赖 | commons-core | 主要是一些公共的工具，公共的类  |
| 实体相关类 | domain-model | orm数据库映射类，以及各个公共的model |
| 防止借口重复提交 | double-submit-spring-boot-starter | 防止借口重复提交starter公共jar包   |
| 文件服务 | file-center | 文件上传服务  |
| 网关服务 | gateway | 统一网关服务，请求入口  |
| 后台管理服务 | manage-center| 后台管理平台 |
| nacos各个服务配置文件目录 | nacos_config_export_20210227092703| nacos各个服务配置文件，可以打成zip的文件上传到nacos服务中 |
| 用户服务 | user-center| 用户相关信息 |



### 初始化工作

(一定要安装redis sentinel zipkin nacos docker )

1.将项目下载下来导入自己的开发工具idea或者eclipse里面

2.导入配置文件nacos_config_export_20210227092703到nacos服务中，修改相关配置，数据库连接，redis配置，sentinel配置，zipkin 配置

3.建库建表，多少sql在相应的工程里面 

4.提前启动 nacos  sentinel  zipkin ，可以到相应的官网下载，

5.maven进行打包，拷贝到服务器上面执行 nohup java -jar jar包名称方可启动，我目前用的是jenkis+docker自动化部署，自己玩手动启动方可

6.项目添加了签名认证拦截，可以在gateway配置文件进行修改开启与否，里面都有注释很详细，开启后接口相对很安全

### 项目部分截图
![jenkins自动化部署](https://images.gitee.com/uploads/images/2021/0505/085119_7202125f_714952.png "Jenkins.png")
![部署相关目录](https://images.gitee.com/uploads/images/2021/0505/085200_e39e9eda_714952.png "docker.png")
![nacos](https://images.gitee.com/uploads/images/2021/0505/085233_6ab0c857_714952.png "nacos.png")
![knife4j自动化文档](https://images.gitee.com/uploads/images/2021/0505/085247_73aa323f_714952.png "kf.png")
![zipkin链路追踪](https://images.gitee.com/uploads/images/2021/0505/085322_b2bfc3e0_714952.png "zilkin.png")
![sentinel熔断控制](https://images.gitee.com/uploads/images/2021/0505/085353_9f0b08c6_714952.png "sentinel.png")


