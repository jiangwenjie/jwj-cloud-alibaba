package com.cloud.model.file;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * FILE_INFO
 * 
 * @author jiangwenjie
 * @version 1.0.0 2020-08-12
 */
@Data
@Entity
@Table(name = "FILE_INFO")
public class FileInfo implements java.io.Serializable {
    /** 版本号 */
    private static final long serialVersionUID = -968057869841242337L;


    /** id */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false, length = 19)
    private Long id;

    /** filename */
    @Column(name = "FILENAME", nullable = true, length = 64)
    private String filename;

    /** bucket */
    @Column(name = "BUCKET", nullable = true, length = 32)
    private String bucket;

    /** expireTime */
    @Column(name = "EXPIRE_TIME", nullable = true, length = 19)
    private Date expireTime;

    /** url */
    @Column(name = "URL", nullable = true, length = 600)
    private String url;

    /** mimeType */
    @Column(name = "MIME_TYPE", nullable = true, length = 36)
    private String mimeType;

    /** size */
    @Column(name = "SIZE", nullable = true, length = 19)
    private Long size;

    /** width */
    @Column(name = "WIDTH", nullable = true, length = 10)
    private Integer width;

    /** height */
    @Column(name = "HEIGHT", nullable = true, length = 10)
    private Integer height;

    /** updateTime */
    @Column(name = "UPDATE_TIME", nullable = true, length = 19)
    private Date updateTime;

    /** createTime */
    @Column(name = "CREATE_TIME", nullable = true, length = 19)
    private Date createTime;


}