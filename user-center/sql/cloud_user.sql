/*
Navicat MySQL Data Transfer

Source Server         : 120.79.157.199
Source Server Version : 50720
Source Host           : 120.79.157.199:3306
Source Database       : cloud_user

Target Server Type    : MYSQL
Target Server Version : 50720
File Encoding         : 65001

Date: 2020-03-18 18:03:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `app_user`
-- ----------------------------
DROP TABLE IF EXISTS `app_user`;
CREATE TABLE `app_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `phone` varchar(13) DEFAULT NULL COMMENT '手机号',
  `username` varchar(40) DEFAULT NULL COMMENT '用户名称',
  `nickname` varchar(40) DEFAULT NULL COMMENT '昵称',
  `password` varchar(50) DEFAULT NULL COMMENT '密码',
  `face_image` varchar(300) DEFAULT NULL COMMENT '头像小图url',
  `face_image_big` varchar(300) DEFAULT NULL COMMENT '头像原图url',
  `qrcode` varchar(300) DEFAULT NULL COMMENT '二维码',
  `mac_id` varchar(50) DEFAULT NULL COMMENT '终端设备id',
  `status` int(11) DEFAULT '1' COMMENT '状态 1表示有效 0 表示无效',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `Index_phone` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COMMENT='用户表';

-- ----------------------------
-- Records of app_user
-- ----------------------------
INSERT INTO `app_user` VALUES ('13', '18929551641', '杰信104634', '111', 'D975F74C5B2AF18F5E954A7845539C9F', 'jwj/M00/00/00/wKgBbl5vbueAe1DvAHwOHO-ynyo615_80x80.png', 'jwj/M00/00/00/wKgBbl5vbueAe1DvAHwOHO-ynyo615.png', 'jwj/M00/00/00/wKgBbl5q5nKADBUGAAABloDJ5xM819.png', 'bc09310fa1b75d9be5a445c58c52c918', '1', '2020-03-13 09:48:06', '2020-03-16 20:19:35');
INSERT INTO `app_user` VALUES ('14', '18929551642', '杰信102572', '小甜甜', 'D975F74C5B2AF18F5E954A7845539C9F', 'jwj/M00/00/00/wKgBbl5q6cWAU23oABOJ9YerUuA752_80x80.png', 'jwj/M00/00/00/wKgBbl5q6cWAU23oABOJ9YerUuA752.png', 'jwj/M00/00/00/wKgBbl5q56mAS9QRAAABhlWrSUM630.png', '3a7128a9e64a090fa72f871d7a6b2dea', '1', '2020-03-13 09:53:18', '2020-03-13 10:02:47');
INSERT INTO `app_user` VALUES ('16', '18929551643', '杰信103184', '杰信103184', 'D975F74C5B2AF18F5E954A7845539C9F', 'jwj/M00/00/00/wKgBbl5rGuyACHlmAHx1azcRvcc559_80x80.png', 'jwj/M00/00/00/wKgBbl5rGuyACHlmAHx1azcRvcc559.png', 'jwj/M00/00/00/wKgBbl5rGiiAFCTzAAABnjfoeo0958.png', 'bc09310fa1b75d9be5a445c58c52c918', '1', '2020-03-13 13:28:44', '2020-03-13 13:32:02');
INSERT INTO `app_user` VALUES ('17', '18929551644', '杰信104815', '杰信104815', 'D975F74C5B2AF18F5E954A7845539C9F', 'jwj/M00/00/00/wKgBbl5vcP2ATko5AAVjg8ueLeE396_80x80.png', 'jwj/M00/00/00/wKgBbl5vcP2ATko5AAVjg8ueLeE396.png', 'jwj/M00/00/00/wKgBbl5vcBSAYLQBAAABhiw4L_c292.png', '3a7128a9e64a090fa72f871d7a6b2dea', '1', '2020-03-16 20:24:22', '2020-03-16 20:28:15');

-- ----------------------------
-- Table structure for `friend_request`
-- ----------------------------
DROP TABLE IF EXISTS `friend_request`;
CREATE TABLE `friend_request` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `send_user_id` bigint(20) DEFAULT NULL COMMENT '发送方id',
  `accept_user_id` bigint(20) DEFAULT NULL COMMENT '接收方id',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COMMENT='好友请求表';

-- ----------------------------
-- Records of friend_request
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_permission`
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `permission` varchar(80) DEFAULT NULL COMMENT '权限标识',
  `name` varchar(60) DEFAULT NULL COMMENT '权限名称',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `Index_permission` (`permission`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COMMENT='系统权限表';

-- ----------------------------
-- Records of sys_permission
-- ----------------------------
INSERT INTO `sys_permission` VALUES ('1', 'back:user:query', '系统用户查询', '2020-03-06 15:44:04', '2020-03-06 15:44:07');
INSERT INTO `sys_permission` VALUES ('2', 'back:user:add', '添加系统用户', '2020-03-06 16:03:34', '2020-03-06 16:03:38');
INSERT INTO `sys_permission` VALUES ('3', 'back:user:update', '修改系统用户', '2020-03-06 17:11:50', '2020-03-06 17:11:52');
INSERT INTO `sys_permission` VALUES ('4', 'back:user:userRole', '用户分配角色', '2020-03-18 17:50:36', '2020-03-18 17:50:36');
INSERT INTO `sys_permission` VALUES ('5', 'back:permission:query', '查询角色信息列表', '2020-03-18 17:52:14', '2020-03-18 17:52:14');
INSERT INTO `sys_permission` VALUES ('6', 'back:permission:add', '添加权限', '2020-03-18 17:52:39', '2020-03-18 17:52:39');
INSERT INTO `sys_permission` VALUES ('7', 'back:permission:delete', '删除权限', '2020-03-18 17:55:15', '2020-03-18 17:55:15');
INSERT INTO `sys_permission` VALUES ('8', 'back:permission:update', '修改权限', '2020-03-18 17:55:43', '2020-03-18 17:55:43');
INSERT INTO `sys_permission` VALUES ('9', 'back:role:query', '查询角色信息列表', '2020-03-18 17:56:16', '2020-03-18 17:56:16');
INSERT INTO `sys_permission` VALUES ('10', 'back:role:add', '添加角色', '2020-03-18 17:56:41', '2020-03-18 17:56:41');
INSERT INTO `sys_permission` VALUES ('11', 'back:role:update', '修改角色', '2020-03-18 17:57:02', '2020-03-18 17:57:02');
INSERT INTO `sys_permission` VALUES ('12', 'back:role:delete', '删除角色', '2020-03-18 17:57:24', '2020-03-18 17:57:24');
INSERT INTO `sys_permission` VALUES ('13', 'back:role:rolePermission', '角色分配权限', '2020-03-18 17:57:46', '2020-03-18 17:57:46');
INSERT INTO `sys_permission` VALUES ('14', 'back:role:roleMenu', '角色分配菜单', '2020-03-18 17:58:05', '2020-03-18 17:58:05');
INSERT INTO `sys_permission` VALUES ('15', 'back:menu:query', '查询菜单列表', '2020-03-18 17:58:56', '2020-03-18 17:58:56');
INSERT INTO `sys_permission` VALUES ('16', 'back:menu:add', '添加菜单', '2020-03-18 17:59:17', '2020-03-18 17:59:17');
INSERT INTO `sys_permission` VALUES ('17', 'back:menu:delete', '删除菜单', '2020-03-18 17:59:34', '2020-03-18 17:59:34');
INSERT INTO `sys_permission` VALUES ('18', 'back:blackIp:query', '查询ip黑名单列表', '2020-03-18 18:01:04', '2020-03-18 18:01:04');
INSERT INTO `sys_permission` VALUES ('19', 'back:blackIp:add', '添加ip黑名单', '2020-03-18 18:01:19', '2020-03-18 18:01:19');
INSERT INTO `sys_permission` VALUES ('20', 'back:blackIp:delete', '删除ip黑名单', '2020-03-18 18:01:46', '2020-03-18 18:01:46');

-- ----------------------------
-- Table structure for `sys_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(25) DEFAULT NULL COMMENT '角色code',
  `name` varchar(60) DEFAULT NULL COMMENT '角色名称',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `Index_code` (`code`),
  UNIQUE KEY `Index_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='角色表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', 'SUPER_ADMIN', '超级管理员', '2020-03-05 19:51:58', '2020-03-05 19:52:02');
INSERT INTO `sys_role` VALUES ('2', 'DEVELOPMENT', '开发人员', '2020-03-06 15:47:13', '2020-03-06 15:47:15');
INSERT INTO `sys_role` VALUES ('3', 'BOSS', '老板', '2020-03-07 14:18:56', '2020-03-07 14:18:59');

-- ----------------------------
-- Table structure for `sys_role_permission`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_permission`;
CREATE TABLE `sys_role_permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sys_role_id` bigint(20) DEFAULT NULL COMMENT '角色id',
  `permission_id` bigint(20) DEFAULT NULL COMMENT '权限id',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `Index_sys_role_id` (`sys_role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='角色权限关系表';

-- ----------------------------
-- Records of sys_role_permission
-- ----------------------------
INSERT INTO `sys_role_permission` VALUES ('1', '2', '1', '2020-03-06 15:48:36');

-- ----------------------------
-- Table structure for `sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(60) DEFAULT NULL COMMENT '用户名称',
  `password` varchar(60) DEFAULT NULL COMMENT '密码',
  `nickname` varchar(60) DEFAULT NULL COMMENT '昵称',
  `head_img_url` varchar(400) DEFAULT NULL COMMENT '头像',
  `phone` varchar(15) DEFAULT NULL COMMENT '手机号',
  `status` int(11) DEFAULT NULL COMMENT '状态 1表示有效 0 表示无效',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `Index_phone` (`phone`),
  UNIQUE KEY `Index_username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='后台管理用户表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'superAdmin', 'superAdmin', null, null, '18929551649', '1', '2020-03-06 16:26:08', '2020-03-06 16:26:10');
INSERT INTO `sys_user` VALUES ('4', 'guest', '123456', '', '', '18929551640', '1', '2020-03-05 18:42:23', '2020-03-06 17:10:02');

-- ----------------------------
-- Table structure for `sys_user_credentials`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_credentials`;
CREATE TABLE `sys_user_credentials` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `login_type` int(11) DEFAULT NULL COMMENT '登录类型 1用户名登录 2手机号登录 3微信登录 4支付宝登录 等等',
  `login_account` varchar(60) DEFAULT NULL COMMENT '账号 根据上面账号类型不同存入的值不同',
  `sys_user_id` bigint(20) DEFAULT NULL COMMENT '用户id',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `Index_login_account` (`login_account`),
  KEY `Index_sys_user_id` (`sys_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='系统用户凭证表';

-- ----------------------------
-- Records of sys_user_credentials
-- ----------------------------
INSERT INTO `sys_user_credentials` VALUES ('1', '1', 'superAdmin', '1', '2020-03-07 11:55:34');
INSERT INTO `sys_user_credentials` VALUES ('2', '2', '18929551649', '1', '2020-03-07 11:55:58');
INSERT INTO `sys_user_credentials` VALUES ('3', '1', 'guest', '4', '2020-03-05 18:42:23');
INSERT INTO `sys_user_credentials` VALUES ('4', '2', '18929551640', '4', '2020-03-05 18:42:23');

-- ----------------------------
-- Table structure for `sys_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sys_user_id` bigint(20) DEFAULT NULL COMMENT '用户id',
  `sys_role_id` bigint(20) DEFAULT NULL COMMENT '角色id',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `Index_sys_user_id` (`sys_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='用户角色关系表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '4', '2', '2020-03-05 19:54:48');

-- ----------------------------
-- Table structure for `user_friend`
-- ----------------------------
DROP TABLE IF EXISTS `user_friend`;
CREATE TABLE `user_friend` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户id',
  `friend_user_id` bigint(20) DEFAULT NULL COMMENT '好友id',
  `remake` varchar(30) DEFAULT NULL COMMENT '备注',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8mb4 COMMENT='用户好友关系表';

-- ----------------------------
-- Records of user_friend
-- ----------------------------
INSERT INTO `user_friend` VALUES ('87', '14', '13', null, '2020-03-13 12:32:23', '2020-03-13 12:32:23');
INSERT INTO `user_friend` VALUES ('88', '13', '14', null, '2020-03-13 12:32:23', '2020-03-13 12:32:23');
INSERT INTO `user_friend` VALUES ('89', '17', '13', null, '2020-03-16 20:26:48', '2020-03-16 20:26:48');
INSERT INTO `user_friend` VALUES ('90', '13', '17', null, '2020-03-16 20:26:48', '2020-03-16 20:26:48');
