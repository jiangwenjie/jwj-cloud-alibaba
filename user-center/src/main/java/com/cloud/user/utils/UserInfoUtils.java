package com.cloud.user.utils;

import com.cloud.common.constants.HeaderConstant;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

public class UserInfoUtils {

    /**
     * 获取用户后台管理用户id
     * @return
     */
    public static Long getAdminUserId() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        return Long.parseLong(request.getHeader(HeaderConstant.ADMINUSERID));
    }

    /**
     * 获取app用户id
     * @return
     */
    public static Long getAppUserId() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        return Long.parseLong(request.getHeader(HeaderConstant.APPUSERID));
    }
}
