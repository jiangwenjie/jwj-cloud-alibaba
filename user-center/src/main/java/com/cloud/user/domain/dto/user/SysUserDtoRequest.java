package com.cloud.user.domain.dto.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@ApiModel(value = "系统用户添加SysUserDtoRequest")
@Data
public class SysUserDtoRequest {

    @NotBlank(message = "用户名不能为空")
    @ApiParam(required=true, name="phone", value="用户名 必填" ,example="王飞")
    private String username;

    @NotBlank(message = "密码不能为空")
    @ApiParam(required=true, name="password", value="密码 必填" ,example="12345")
    @ApiModelProperty(value="密码")
    private String password;

    @NotBlank(message = "手机号不能为空")
    @ApiParam(required=true, name="phone", value="手机号 必填"  ,example="18956445468")
    private String phone;

    @ApiParam(required=false, name="nickname", value="昵称 非必填",example="jack画画")
    private String nickname;

    @ApiParam(required=false, name="headImgUrl", value="用户头像url 非必填",example="https://gimg2.baidu.com/ima06114735_2.jpg")
    private String headImgUrl;
}
