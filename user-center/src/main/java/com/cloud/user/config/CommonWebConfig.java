package com.cloud.user.config;

import com.cloud.user.interceptor.AuthorityInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @ClassName : CommonWebConfig  //类名
 * @Description : 拦截器配置类  //描述
 * @Author : JiangWenJie  //作者
 * @Date: 2021-02-04 11:42  //时间
 */
@Configuration
public class CommonWebConfig implements WebMvcConfigurer {


    @Autowired
    private PathNoLoginConfig pathNoLoginConfig;


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //注册连接器AuthorityInterceptor
        InterceptorRegistration registration = registry.addInterceptor(new AuthorityInterceptor());
        registration.addPathPatterns("/api/admin/**");//所有/api/admin/**路径都被拦截 后台管理平台权限相关
        registration.excludePathPatterns(pathNoLoginConfig.getIgnoredPath());
    }


}
