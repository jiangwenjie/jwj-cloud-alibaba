package com.cloud.user.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * 忽略url配置类
 */
@Configuration
@ConfigurationProperties("login")
@Data
@RefreshScope
public class PathNoLoginConfig {

    /**
     * 不需要登陆认证的路径
     */
    private List<String> ignoredPath;


}