package com.cloud.admin.config;

import com.github.xiaoymin.knife4j.core.util.CollectionUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.annotation.Order;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;
import java.util.List;

/**
 * @ClassName : SwaggerConfig  //类名
 * @Description : swagger稳定配置类  //描述
 * @Author : JiangWenJie  //作者
 * @Date: 2022-02-03 13:42  //时间
 */
@Configuration
@EnableSwagger2WebMvc
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {

    @Bean(value = "adminRestApi")
    @Order(value = 1)
    public Docket groupRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(groupApiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.cloud.admin.controller"))
                .paths(PathSelectors.any())
                .build()
                .securityContexts(CollectionUtils
                        .newArrayList(securityContext(),securityContext1()))
                            .securitySchemes(CollectionUtils
                                .<SecurityScheme>newArrayList(apiKey0(),apiKey1(),apiKey2(),apiKey3(),apiKey4()));

    }

    private ApiInfo groupApiInfo(){
        return new ApiInfoBuilder()
                .title("后台管理中心api接口文档")
                .description(
                        "header里面定义的参数如下：" +
                        "timestamp:时间戳验证；" +
                        "requestId:保证每次请求都是唯一；" +
                        "sign:签名；" +"token:登录令牌；"+
                        "channelId:渠道来源 ios android h5 web ")
                .termsOfServiceUrl("http://www.xxx.com/")
                .contact(new Contact("蒋文杰","https://github.com/jiangwenjie1989","619594586@qq.com"))
                .version("1.0.0")
                .build();
    }

    private ApiKey apiKey0() {
        return new ApiKey("Authorization", "登录认证token 登录后后台会返回", "header");
    }
    private ApiKey apiKey1() {
        return new ApiKey("sign", "sign 按照约定好的规则进行计算", "header");
    }
    private ApiKey apiKey2() {
        return new ApiKey("timestamp", "时间戳长度13位 例如:1612336078216 ", "header");
    }
    private ApiKey apiKey3() {
        return new ApiKey("requestId", "保证每次请求都是唯一随机字符串,最好用uuid", "header");
    }
    private ApiKey apiKey4() {
        return new ApiKey("channelType", "渠道来源ios  android  h5  web", "header");
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(defaultAuth())
                .forPaths(PathSelectors.regex("/.*"))
                .build();
    }
    private SecurityContext securityContext1() {
        return SecurityContext.builder()
                .securityReferences(defaultAuth1())
                .forPaths(PathSelectors.regex("/.*"))
                .build();
    }

    List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return CollectionUtils.newArrayList(new SecurityReference("BearerToken", authorizationScopes));
    }
    List<SecurityReference> defaultAuth1() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return CollectionUtils.newArrayList(new SecurityReference("BearerToken1", authorizationScopes));
    }
}
