package com.cloud.admin.vo.menu;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.List;

@ApiModel(value = "菜单UserMenuListVO")
@Data
public class UserMenuListVO extends MenuInfoVO{

    @ApiModelProperty(value="二级菜单")
    private List<MenuInfoVO> twoMenuList;

}
