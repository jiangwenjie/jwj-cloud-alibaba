/*
Navicat MySQL Data Transfer

Source Server         : 120.79.157.199
Source Server Version : 50720
Source Host           : 120.79.157.199:3306
Source Database       : cloud_admin

Target Server Type    : MYSQL
Target Server Version : 50720
File Encoding         : 65001

Date: 2020-03-18 18:04:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `black_ip`
-- ----------------------------
DROP TABLE IF EXISTS `black_ip`;
CREATE TABLE `black_ip` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(20) DEFAULT NULL COMMENT 'ip地址',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COMMENT='ip黑名单表';

-- ----------------------------
-- Records of black_ip
-- ----------------------------
INSERT INTO `black_ip` VALUES ('1', '120.135.1.23', '2020-03-08 20:46:19');
INSERT INTO `black_ip` VALUES ('2', '120.135.1.130', '2020-03-08 20:52:03');
INSERT INTO `black_ip` VALUES ('3', '120.135.1.131', '2020-03-08 21:05:45');
INSERT INTO `black_ip` VALUES ('4', '120.135.1.132', '2020-03-08 21:10:03');

-- ----------------------------
-- Table structure for `menu`
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父菜单id 默认值是0表示没有父id 也就是顶级目录',
  `name` varchar(50) DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(100) DEFAULT NULL COMMENT '菜单url',
  `css` varchar(80) DEFAULT NULL COMMENT '菜单样式',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COMMENT='菜单表';

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', '0', '系统设置', '', 'fa-gears', '1', '2020-03-08 12:47:28', '2020-03-08 12:47:28');
INSERT INTO `menu` VALUES ('2', '1', '菜单', 'pages/menu/menuList.html', 'fa-windows', '2', '2020-03-08 12:49:06', '2020-03-08 12:49:06');
INSERT INTO `menu` VALUES ('3', '1', '角色', 'pages/role/roleList.html', 'fa-cubes', '3', '2020-03-08 12:49:48', '2020-03-08 12:49:48');
INSERT INTO `menu` VALUES ('4', '1', '权限', 'pages/permission/permissionList.html', 'fa-align-justify', '4', '2020-03-08 12:50:24', '2020-03-08 12:50:24');
INSERT INTO `menu` VALUES ('5', '0', '用户管理', '', 'fa-user', '4', '2020-03-08 12:51:08', '2020-03-08 12:51:08');

-- ----------------------------
-- Table structure for `role_menu`
-- ----------------------------
DROP TABLE IF EXISTS `role_menu`;
CREATE TABLE `role_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sys_role_id` bigint(20) DEFAULT NULL COMMENT '角色id',
  `menu_id` bigint(20) DEFAULT NULL COMMENT '菜单id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色菜单关系表';

-- ----------------------------
-- Records of role_menu
-- ----------------------------
