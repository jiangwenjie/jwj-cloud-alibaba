package com.cloud.submit.autoconfigure;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;


/**
 * <p>ClassName : CacheLock  </p>
 * <p>Description : Lock注解定义  </p>
 * @author : JiangWenJie  
 * @since: 2020-08-18 14:21  </p>
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface CacheLock {

	/**
	 * redis 锁key的前缀
	 *
	 * @return redis 锁key的前缀
	 */
	String prefix() default "";

	/**
	 * 过期秒数,默认为5秒
	 *
	 * @return  int 轮询锁的时间
	 */
	int expire() default 5;

	/**
	 * 超时时间单位
	 *
	 * @return TimeUnit 秒
	 */
	TimeUnit timeUnit() default TimeUnit.SECONDS;

	/**
	 * <p>Key的分隔符（默认 :）</p>
	 * <p>生成的Key：N:SO1008:500</p>
	 *
	 * @return String Key的分隔符
	 */
	String delimiter() default ":";
}