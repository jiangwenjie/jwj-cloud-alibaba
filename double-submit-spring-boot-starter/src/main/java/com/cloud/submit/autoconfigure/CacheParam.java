package com.cloud.submit.autoconfigure;
import java.lang.annotation.*;


/**
 * <p>@ClassName : CacheParam  </P>
 * <p>Description : Param参数注解定义 </P>
 * @author : JiangWenJie  
 * @since: 2020-08-18 14:21  
 */
@Target({ElementType.PARAMETER, ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface CacheParam {

    /**
     * 字段名称
     *
     * @return String 名称
     */
    String name() default "";
}
