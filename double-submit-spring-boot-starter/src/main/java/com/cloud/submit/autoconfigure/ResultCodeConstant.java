package com.cloud.submit.autoconfigure;

public class ResultCodeConstant {

    public static final Integer RESULT_FAIL = 500;

    /**
     * 重复提交
     */
    public static final Integer RESULT_FAIL_REPETITION_SUBMIT = 10010;

}
