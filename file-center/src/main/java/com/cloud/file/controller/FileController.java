package com.cloud.file.controller;


import com.cloud.common.constants.SwggerCommonTags;
import com.cloud.common.response.ApiResponse;
import com.cloud.file.service.FileService;
import com.cloud.file.domain.vo.FileUpInfoVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName : FileController  //类名
 * @Description : 文件上传oss服务控制层  //描述
 * @Author : JiangWenJie  //作者
 * @Date: 2021-02-04 15:08  //时间
 */
@Api(tags = SwggerCommonTags.SYS_FILE_MODULE)
@RequestMapping(value = "/api/upload")
@RestController
public class FileController {

    @Autowired
    private FileService fileService;


    @ApiOperation(value = "获取文件上传token权限信息接口", httpMethod = "GET")
    @RequestMapping(value = "/common/token",method= RequestMethod.GET)
    public ApiResponse<FileUpInfoVO> getCommonToken() throws Exception {
        return fileService.getCommonToken();
    }


}
