package com.cloud.file;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @ClassName : FileApplication  //类名
 * @Description : 文件上传oss服务  //描述
 * @Author : JiangWenJie  //作者
 * @Date: 2021-02-05 15:08  //时间
 */
@EnableDiscoveryClient
@SpringBootApplication
@EnableJpaRepositories(basePackages="com.cloud.file.repository")
@EntityScan(basePackages="com.cloud.model.file")
@ComponentScan(basePackages="com.cloud.*")
public class FileApplication {

    public static void main(String[] args) {
        SpringApplication.run(FileApplication.class, args);
    }

}
