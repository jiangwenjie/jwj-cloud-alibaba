package com.cloud.file.domain.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class FileUpInfoVO implements Serializable {

    @ApiModelProperty(value="访问id")
    private String accessId;

    @ApiModelProperty(value="上传凭证")
    private String policy;

    @ApiModelProperty(value="签名")
    private String signature;

    @ApiModelProperty(value="指定上传目录")
    private String dir;

    @ApiModelProperty(value="域名")
    private String host;

    @ApiModelProperty(value="时间戳")
    private String expire;

    @ApiModelProperty(value="回调body")
    private String callback;

}
