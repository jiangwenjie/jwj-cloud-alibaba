package com.cloud.file.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class FileInfoDtoRequest {

    @ApiModelProperty(value="文件名称")
    private String filename;

    @ApiModelProperty(value="存储位置")
    private String bucket;

    @ApiModelProperty(value="访问url")
    private String url;

    @ApiModelProperty(value="文件大小")
    private Long size;

    @ApiModelProperty(value="宽度")
    private Long width;

}
