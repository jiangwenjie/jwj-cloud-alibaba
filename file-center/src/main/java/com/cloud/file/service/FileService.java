package com.cloud.file.service;

import com.cloud.common.response.ApiResponse;
import com.cloud.file.domain.vo.FileUpInfoVO;

public interface FileService {

    /**
     * 获取文件上传token权限信息接口
     * @return
     */
    ApiResponse<FileUpInfoVO> getCommonToken()throws Exception;

}
