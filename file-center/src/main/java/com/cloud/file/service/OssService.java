package com.cloud.file.service;

import com.cloud.common.response.SimpleResponse;

public interface OssService {

    /**
     * 出来阿里返回数据
     * @param requestBody
     * @param autorization
     * @param pubKeyUrl
     * @param queryString
     * @param uri
     * @return
     */
    public SimpleResponse handlerCallbackResult(String requestBody, String autorization, String pubKeyUrl,
                                                String queryString, String uri);
}
