package com.cloud.file.dao;

import com.cloud.file.repository.FileInfoRepository;
import com.cloud.model.file.FileInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class FileInfoDao {

    @Autowired
    private FileInfoRepository fileInfoRepository;


    public FileInfo save(FileInfo file) {
        return fileInfoRepository.save(file);
    }
}
