package com.cloud.file.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@ConfigurationProperties(prefix = "oss")
@Component
public class AliOSSConfig {
	
	private String host;
	
	private String endpoint;
	 
	private String bucket;
	
	private String priHost;
	
	private String priBucket;
	
	private String priCallback;
	
	private String accessKeyId;
	
	private String accessKeySecret; 
 
	private String fileDir;
	
	private String entDir;
	
	private String callback;
	
	private String userDir;
	
	private String roleArn;
	
	private String stsEndpoint;
	
	private Long stsDuration;
	
	private Long storageSpace;
}
