package com.cloud.file.config;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName : WebConfiguration  //类名
 * @Description : bean配置类  //描述
 * @Author : JiangWenJie  //作者
 * @Date: 2021-02-05 15:08  //时间
 */
@Configuration
public class WebConfiguration {

	@Bean
	public OSS ossClient(AliOSSConfig config) {
		return new OSSClientBuilder().build(config.getAccessKeyId(), config.getAccessKeyId(),
				config.getAccessKeySecret());
	}


}
