package com.cloud.file.repository;

import com.cloud.model.file.FileInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FileInfoRepository extends JpaRepository<FileInfo, Long> {


}
