package com.cloud.common.constants;

/**
 * 请求头常量参数
 */
public class HeaderConstant {

    /**
     * 请求头中获取令牌
     */
    public final static String AUTHORIZATION = "Authorization";

    /**
     * 渠道来源ios android h5 web
     */
    public final static String CHANNELTYPE = "channelType";


    /**
     * 证每次请求都是唯一随机字符串,最好用uuid
     */
    public final static String REQUESTID = "requestId";


    /**
     * sign 按照约定好的规则进行计算
     */
    public final static String SIGN = "sign";

    /**
     * sign 签名约定好的盐 作为一个请求参数进行拼接，不用放在请求头header里面
     */
    public final static String SIGNSALT = "salt";


    /**
     * 时间戳长度13位 例如:1612336078216
     */
    public final static String TIMESTAMP = "timestamp";

    /**
     * 后台管理用户id
     */
    public final static String ADMINUSERID = "adminUserId";


    /**
     * 后台管理用户名称
     */
    public final static String ADMINUSERNAME = "adminUsername";

    /**
     * 后台管理用户状态
     */
    public final static String ADMINUSERSTATUS = "adminUserStatus";

    /**
     * app用户id
     */
    public final static String APPUSERID = "appUserId";


    /**
     * app用户名称
     */
    public final static String APPUSERNAME = "appUsername";

    /**
     * app用户状态
     */
    public final static String APPUSERSTATUS = "appUserStatus";

}
