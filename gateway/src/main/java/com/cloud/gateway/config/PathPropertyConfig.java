package com.cloud.gateway.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * 忽略url配置类
 */
@Configuration
@ConfigurationProperties("path")
@Data
@RefreshScope
public class PathPropertyConfig {

    /**
     * 不需要登陆认证的路径
     */
    private List<String> ignoredTokenPath;


}