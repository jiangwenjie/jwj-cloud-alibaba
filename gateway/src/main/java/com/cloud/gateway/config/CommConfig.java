package com.cloud.gateway.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;

@Configuration
public class CommConfig {




    /**
     *具体原理就是使用一个全局的Route 给他塞了readBody ReadBodyPredicateFactory就会解析 放到
     * exchange.getAttributes()中 ,yml里面的路由地址,和这里配置保持一致
     * @param builder
     * @return
     */
    @Bean
    public RouteLocator requestBodyCacheRoute(RouteLocatorBuilder builder) {
        RouteLocatorBuilder.Builder routes = builder.routes();
        RouteLocatorBuilder.Builder serviceProvider = routes
                .route("requestBodyCacheRoute",
                        r -> r.method(HttpMethod.POST)
                                .and()
                                .readBody(String.class, readBody -> true)
                                .and()
                                .path("user-center")
                                .uri("lb://user-center"))

                .route("requestBodyCacheRoute",
                        r -> r.method(HttpMethod.POST)
                                .and()
                                .readBody(String.class, readBody -> true)
                                .and()
                                .path("manage-center")
                                .uri("lb://manage-center"))

                .route("requestBodyCacheRoute",
                        r -> r.method(HttpMethod.POST)
                                .and()
                                .readBody(String.class, readBody -> true)
                                .and()
                                .path("file-center")
                                .uri("lb://file-center"));

        RouteLocator routeLocator = serviceProvider.build();
        return routeLocator;
    }
}
