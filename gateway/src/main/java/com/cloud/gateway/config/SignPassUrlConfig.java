package com.cloud.gateway.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * 忽略url配置类
 */
@Configuration
@ConfigurationProperties("sign")
@Data
@RefreshScope
public class SignPassUrlConfig {

    /**
     * 不需要登陆认证的路径
     */
    private List<String> ignoredSignPath;

    /**
     * 是否开启签名认证
     */
    @Value("${sign.flag}")
    private Boolean signFlag;

    /**
     * 签名加盐
     */
    @Value("${sign.salt}")
    private String signSalt;

}